#include <iostream>
#include <fstream>
#include <cassert>
#include <string>
#include <vector>

using namespace std;

/* Opis zadania znajduje sie w pliku zadanie.txt */

// Enigma umozliwia szyfrowanie i deszyfrowanie napisow oraz plikow
class Enigma {

    private:
        // pole rodzaj_szyfru okresla jaki szyfr bedzie stosowany przez enigme
        string rodzaj_szyfru;
        // TO-DO (1): pole klucz definiuje klucz szyfru, jego znaczenie zalezy od wybranej metody szyfrowania
        int klucz;
    

        // TO-DO (3): dodaj metode szyfrujaca szyfrem przestawnym
        string szyfr_przestawny(string napis)
        {   
            string aaa = napis; 
            vector<bool> check;  

            for(unsigned int i =0; i < napis.length() - klucz; i++) 
                check.push_back(false);  

            for(unsigned int i = 0 ; i < napis.length() - klucz; i++)
            {
                if (!check[i]) {
                    aaa[i] = napis [i+klucz];
                    aaa[i+klucz] = napis [i];
                    check[i] = true; 
                    check[i+klucz] = true;
                }
            } 
            return aaa;
        }

        // metoda deszyfrujaca szyfr przestawny
        string deszyfruj_przestawnie(string napis) {
            // TO-DO (6): zaimplementuj deszyfrowanie szyfru przestawnego
            return szyfr_przestawny(napis);

        }
        
        //metoda szyfrujaca szyfrem cezara 
        void SzyfrCezara(string szyfr, int klucz)
        {
            for (int i = 0; i < szyfr.length(); i++)
		{
			if (szyfr[i] >= 65 && szyfr[i] <= 90)
			{
				szyfr[i] += klucz;
				while (szyfr [i] > 90) szyfr[i]-=26;
                                while (szyfr [i] < 65) szyfr[i]+=26;
			}
			
			else if (szyfr[i] >= 97 && szyfr[i] <= 122)
			{
				szyfr[i] += klucz;
				while (szyfr [i] > 122) szyfr[i]-=26;
                                while (szyfr [i] < 97) szyfr[i]+=26;
			}
		    }
            return szyfr;
        }


        // TO-DO (9): metoda deszyfrujaca szyfr cezara

    public:
 
        // konstruktor domyslny, obiekt stworzony bez podawania argumentow inicjalizacji
        // nie bedzie modyfikowal napisow
        Enigma() {
            rodzaj_szyfru = "brak";
        }

        // TO-DO (2): tu dodaj konstruktor klasy przyjmujacy dwa argumenty
        Enigma(string szyfr, int k) {
            rodzaj_szyfru = szyfr;
            klucz = k;
        }


        // TO-DO (4, 10): metoda szyfrujaca pojedynczy napis
        string szyfruj(string napis) {
            string zaszyfrowany;
            if (rodzaj_szyfru == "przestawny"){
                zaszyfrowany = szyfr_przestawny(napis);
                return zaszyfrowany;
            }
            else{
                return napis;
            }
        }


        // metoda deszyfrujaca pojedynczy napis
        string deszyfruj(string zaszyfrowany) {
            string odszyfrowany = zaszyfrowany;

            // wybor rodzaju szyfru
            if (rodzaj_szyfru == "przestawny") {
                odszyfrowany = deszyfruj_przestawnie(zaszyfrowany);

            } else if (rodzaj_szyfru == "cezara") {
                // TO-DO (11): tu dodaj wywolanie metody prywatnej dszyfrujacej szyfr cezara

            } 

            return odszyfrowany; 
        }


        // metoda szyfrujaca plik
        void szyfruj(string plik_wejsciowy, string plik_wyjsciowy) {
            // pliki
            ifstream wej(plik_wejsciowy);
            ofstream wyj(plik_wyjsciowy);

            // do tej zmiennej bedziemy wczytywali linie z pliku
            string linijka;

            // sprawdzenie czy udalo sie otworzyc pliki
            if ( !wej || !wyj ) {
                cout << "Nie udalo sie otworzyc plikow. Sprawdz nazwy." << endl;
                return;
            }

            while ( ! wej.eof() ) {
                getline(wej, linijka);
                wyj << szyfruj(linijka) << endl;
            }
        }

        // TO-DO: zaimplementuj metode deszyfrujaca plik

};

int main() {

    // test szkieletu Enigmy
    // ---------------------
    cout << "Test zepsutej enigmy..." << endl;
    Enigma zepsuta_enigma;
    assert( zepsuta_enigma.deszyfruj("Ten napis nie powinien ulec zmianie.") == "Ten napis nie powinien ulec zmianie.");
    
    // test z plikami
    zepsuta_enigma.szyfruj("test-szyfrowania.txt", "wyjscie-zepsutej-enigmy.txt");

    // pliki wejsciowy i wyjsciowy do porownani
    ifstream plik_testowy_plan_dzialania("test-szyfrowania.txt");
    ifstream plik_wyjscia_zepsutej_enigmy("wyjscie-zepsutej-enigmy.txt");
    
    assert ( plik_testowy_plan_dzialania );
    assert ( plik_wyjscia_zepsutej_enigmy );

    // wczytanie plikow
    vector<string> wejscie_zepsutej_enigmy, wyjscie_zepsutej_enigmy;
    string linijka;

    while ( ! plik_testowy_plan_dzialania.eof() ) {
        getline(plik_testowy_plan_dzialania, linijka);
        wejscie_zepsutej_enigmy.push_back(linijka);  
    }

    while ( ! plik_wyjscia_zepsutej_enigmy.eof() ) {
        getline(plik_wyjscia_zepsutej_enigmy, linijka);
        wyjscie_zepsutej_enigmy.push_back(linijka);               
    }
   
    // sprawdzenie zawartosci
    for (unsigned int i = 0; i < wejscie_zepsutej_enigmy.size(); i++ ) {
        assert(wejscie_zepsutej_enigmy[i]  == wyjscie_zepsutej_enigmy[i]);
    }
    
    cout << "Zepsuta enigma nic nie szyfryje. Ok." << endl;

    // test szyfru przestawnego
    cout << endl << "Test szyfru przestawnego..." << endl;
    Enigma enigma_przestawna("przestawny", 1);
    string zaszyfrowany_przestawnie = enigma_przestawna.szyfruj("Ala ma kota");

    cout << "Tekst zaszyfrowany to: '" << zaszyfrowany_przestawnie << "'." << endl;
    assert( zaszyfrowany_przestawnie == "lA aamk toa");
    cout << "Ok." << endl;

    cout << "Tekst odszyfrowany to: '" << enigma_przestawna.deszyfruj(zaszyfrowany_przestawnie) << "'." << endl;
    assert( enigma_przestawna.deszyfruj(zaszyfrowany_przestawnie) == "Ala ma kota" );
    cout << "Ok." << endl;

    // TO-DO (12): uzupeĹ‚nij testy szyfru cezara podobnie jak jest to zrobione dla szyfru przestawnego
    cout << endl << "Test szyfru cezara..." << endl;
    Enigma enigma_cezara("cezara", 2);
    string zaszyfrowany_cezarem = enigma_cezara.szyfruj("Ala ma kota z psem.");

    cout << "Tekst zaszyfrowany to: '" << zaszyfrowany_cezarem << "'." << endl;
    assert( zaszyfrowany_cezarem == "Cnc oc mqvc b rugo.");
    cout << "Ok." << endl;

    cout << "Tekst odszyfrowany to: '" << enigma_cezara.deszyfruj(zaszyfrowany_cezarem) << "'." << endl;
    assert( enigma_cezara.deszyfruj(zaszyfrowany_cezarem) == "Ala ma kota z psem." );
    cout << "Ok. ;) ;)" << endl;


    // TO-DO (14): Zaszyfruj plik tajna-notatka.txt szyfrem cezara o kluczu 7, 
    // plik wyjsciowy nazwij notatka-code1.txt


    // TO-DO (15): Zaszyfruj plik tajna-notatka.txt szyfrem przestawnym o kluczu 4, 
    // plik wyjsicowy nazwij notatka-code2.txt


    // TO-DO (16): Odszyfruj oba pliki i sprawdz czy maja taka sama zawartosc, jak 
    // orginalna notatka.

    

    return 0;
    
}
